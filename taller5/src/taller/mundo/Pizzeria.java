package taller.mundo;

import taller.estructuras.Heap;

public class Pizzeria 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	private Heap<Pedido> porCocinar;
	/**
	 * Getter de pedidos recibidos
	 */
	public Heap<Pedido> getPedidosRecibidos()
	{
		return porCocinar;
	}
 	/** 
	 * Heap de elementos por despachar
	 */
	private Heap<Pedido> porDespachar;
	/**
	 * Getter de elementos por despachar
	 */
	public Heap<Pedido> getElementosPorDespachar()
	{
		return porDespachar;
	} 
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		porCocinar = new Heap<Pedido>(1);
		porDespachar = new Heap<Pedido>(1);
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		Pedido pedido = new Pedido(precio, nombreAutor, cercania);
		porCocinar.add(pedido);
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		Pedido pedido = (Pedido) porCocinar.poll();
		pedido.cambiarEstado(true);
		porDespachar.add(pedido);
		return  pedido;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		Pedido pedido = (Pedido) porDespachar.poll();
	    return  pedido;
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido[] pedidosRecibidosList()
     {
    	
        return (Pedido[]) porCocinar.sort();
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido[] colaDespachosList()
     {
    	 return (Pedido[]) porDespachar.sort();
     }
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
}
