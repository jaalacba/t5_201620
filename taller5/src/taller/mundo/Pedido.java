package taller.mundo;

public class Pedido implements Comparable<Pedido>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	
	// Mucho más sencillo que agregar un min-heap a la clase heap de Princeton-
	// Cuándo saco los pedidos del primer heap, su concepto de orden cambia.
	private boolean aDespachar;
		
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 */
	public Pedido(double pPrecio, String autor, int pCercania)
	{
		precio = pPrecio;
		autorPedido = autor;
		cercania = pCercania;
		aDespachar = false;
	}
	
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}
	
	public void cambiarEstado(boolean heh)
	{
		aDespachar = heh;
	}
	


	@Override
	public int compareTo(Pedido arg0) {
		if(aDespachar == false)
		{
			double satan =  this.getPrecio() - arg0.getPrecio();
			return aInt(satan);
		}
		else
		{
			int satan =  this.getCercania() - arg0.getCercania();
			return satan;
		}
		
	}
	
	
	private int aInt(double heh)
	{
		if(heh > 0)
		{
			return 1;
		}
		else if(heh == 0)
		{
			return 0;
		}
		else
		{
			return -1;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
