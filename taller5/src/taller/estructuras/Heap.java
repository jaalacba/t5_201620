package taller.estructuras;
/**
 * Código libre de la universidad de Princeton
 * @author Princeton.
 *
 * @param <T>
 */
public class Heap<T extends Comparable<T>> implements IHeap<T> {


	private Comparable<T>[] pq; 
	private int size;
	
	public Heap(int init)
	 {
		pq =  new Comparable[init + 1]; 
		size = 0;
	 }
	
	
	
	//El polimorfismo me ganó.
    public Comparable<T>[] sort() {
        Comparable[] whatEverHappened = pq.clone(); 
    	int n = size;
        
        for (int k = n/2; k >= 1; k--)
            sink(whatEverHappened, k, n);
        while (n > 1) {
            exch(whatEverHappened, 1, n--);
            sink(whatEverHappened, 1, n);
        }
        return whatEverHappened;
    }
    
	@Override
	public void add(Comparable elemento) {
		if (size >= pq.length - 1) resize(2 * pq.length);
		
		 pq[++size] = elemento;
		 siftUp();
		
	}
	


    private void resize(int capacity) {
        assert capacity > size;
        Comparable[] temp = new Comparable[capacity];
        for (int i = 1; i <= size; i++) {
            temp[i] = pq[i];
        }
        pq = temp;
    }
    
	private boolean less(int i, int j)
	{ 
		return pq[i].compareTo((T) pq[j]) < 0; 
	}
	
	@Override
	public T peek() {
		
		return (T) pq[1];
	}

	@Override
	public T poll() {
		Comparable<T> max = pq[1]; // Retrieve max key from top.
		 exch(1, size--); // Exchange with last item.
		 pq[size+1] = null; // Avoid loitering.
		 siftDown(); // Restore heap property.
		 return (T) max;
	}
	
	private void exch(int i, int j)
	{
	Comparable<T> t = pq[i];
	pq[i] = pq[j];
	pq[j] = t;
	}

	@Override
	public int size() {
		return size; 
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void siftUp() {
		int k = size -1;
		while (k > 1 && less(k/2, k))
		 {
		 exch(k/2, k);
		 k = k/2;
		 } 
		
	}

	@Override
	public void siftDown() {
		//sink desde uno. Hice 
		int k = 1;
		while (2*k <= size)
		 {
		 int j = 2*k;
		 if (j < size && less(j, j+1)) j++;
		 if (!less(k, j)) break;
		 exch(k, j);
		 k = j;
		 } 
		
	}
    private boolean less(Comparable[] tk, int i, int j) {
        return tk[i-1].compareTo(tk[j-1]) < 0;
    }
    
    private void exch(Comparable<T>[] tk, int i, int j) {
        Comparable<T> swap = tk[i-1];
        tk[i-1] = tk[j-1];
        tk[j-1] = swap;
    }


    private void sink(Comparable<T>[] tk, int k, int n) {
        while (2*k <= n) {
            int j = 2*k;
            if (j < n && less(tk, j, j+1)) j++;
            if (!less(tk, k, j)) break;
            exch(tk, k, j);
            k = j;
        }
    }

}
